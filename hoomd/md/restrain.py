# -- start license --
# Highly Optimized Object-oriented Many-particle Dynamics -- Blue Edition
# (HOOMD-blue) Open Source Software License Copyright 2009-2014 The Regents of
# the University of Michigan All rights reserved.

# HOOMD-blue may contain modifications ("Contributions") provided, and to which
# copyright is held, by various Contributors who have granted The Regents of the
# University of Michigan the right to modify and/or distribute such Contributions.

# You may redistribute, use, and create derivate works of HOOMD-blue, in source
# and binary forms, provided you abide by the following conditions:

# * Redistributions of source code must retain the above copyright notice, this
# list of conditions, and the following disclaimer both in the code and
# prominently in any materials provided with the distribution.

# * Redistributions in binary form must reproduce the above copyright notice, this
# list of conditions, and the following disclaimer in the documentation and/or
# other materials provided with the distribution.

# * All publications and presentations based on HOOMD-blue, including any reports
# or published results obtained, in whole or in part, with HOOMD-blue, will
# acknowledge its use according to the terms posted at the time of submission on:
# http://codeblue.umich.edu/hoomd-blue/citations.html

# * Any electronic documents citing HOOMD-Blue will link to the HOOMD-Blue website:
# http://codeblue.umich.edu/hoomd-blue/33

# * Apart from the above required attributions, neither the name of the copyright
# holder nor the names of HOOMD-blue's contributors may be used to endorse or
# promote products derived from this software without specific prior written
# permission.

# Disclaimer

# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND/OR ANY
# WARRANTIES THAT THIS SOFTWARE IS FREE OF INFRINGEMENT ARE DISCLAIMED.

# IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
# INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
# OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# -- end license --

from hoomd import _hoomd
from hoomd.md import _md
from hoomd.md import force;
import hoomd;

## Position restraining potential
#
# The Hamiltonian is augmented with a harmonic potential calculated based on the distance between the current and
# initial positions of particles. This effectively allows particles to have nearly fixed position while still retaining
# their degrees of freedom.
# \f[
# U = \frac{1}{2} \mathbf{k} \mathbf{\Delta r} \mathbf{\Delta r}^T
# \f]
# The strength of the potential depends on a spring constant \f$\mathbf{k}\f$ so that the particle position can be
# restrained in any of the three coordinate directions. If \f$\mathbf{k}\f$ is very large, the particle position is
# essentially constrained. However, because the particles retain their degrees of freedom, shorter integration timesteps
# must be taken for large \f$\mathbf{k}\f$ to maintain stability.
class position(force._force):
    ## Add a harmonic restraining potential based on initial position
    # \param group Group of atoms to apply restraint to
    # \param kx Force constant in x-direction
    # \param ky Force constant in y-direction
    # \param kz Force constant in z-direction
    # \param name (optional) name for potential
    #
    # \b Examples
    # \code
    # restrain.position(group=group.all(), kx=1.0, ky=0.0, kz=3.0)
    # respos = restrain.position(group=wall, kx=100.0, ky=100.0, kz=100.0)
    # \endcode
    def __init__(self, group, kx, ky, kz, name=""):
        hoomd.util.print_status_line();

        # initialize the base class
        force._force.__init__(self,name)

        # create the c++ mirror class
        if not hoomd.context.exec_conf.isCUDAEnabled():
            self.cpp_force = _md.PositionRestraintCompute(hoomd.context.current.system_definition,group.cpp_group,kx,ky,kz)
        else:
            self.cpp_force = _md.PositionRestraintComputeGPU(hoomd.context.current.system_definition,group.cpp_group,kx,ky,kz)

        hoomd.context.current.system.addCompute(self.cpp_force, self.force_name)
        
    ## \internal
    # \var cpp_force
    # \brief C++ object for position restraint

    ## Set the force constants for the restraining potential
    # \param kx Force constant in x-direction
    # \param ky Force constant in y-direction
    # \param kz Force constant in z-direction
    #
    # \b Examples
    # \code
    # respos.set_params(kx=3.0, ky=4.0, kz=0.0)
    # \endcode
    def set_params(self, kx, ky, kz):
        self.cpp_force.setForceConstant(kx, ky, kz)

    ## Set the reference positions for the harmonic restraints
    #
    # \b Examples
    # \code
    # respos.set_reference_positions(ref_pos=ref_pos)
    # \endcode
    def set_reference_positions(self, ref_pos):
        # append each particle position
        for i in range(0, len(ref_pos)):
            self.set_position(i,ref_pos[i,:])

    ## \internal
    def set_position(self, i, pos):
        xyz = hoomd.make_scalar4(pos[0],pos[1],pos[2],0.0);
        self.cpp_force.setPosition(i,xyz)
            
    ## \internal
    def update_coeffs(self):
        pass

## Orientation restraining potential
#
# The Hamiltonian is augmented with a \f$D_{\infty h}$\f potential calculated based on the angle between the current and
# initial orientations of particles. This effectively allows particles to have nearly fixed orientation while still retaining
# their degrees of freedom.
# \f[
# U = \mathbf{k} \sin^2 \psi
# \f]
# where \psi is the angle between the current and reference orientation
class orientation(force._force):
    ## Add a restraining potential based on initial orientation
    # \param group Group of atoms to apply restraint to
    # \param k Force constant
    # \param name (optional) name for potential
    #
    # \b Examples
    # \code
    # restrain.orientation(group=group.all(), k=1.0)
    # resor = restrain.orientation(group=wall, k=100.0)
    # \endcode
    def __init__(self, group, k, name=""):
        hoomd.util.print_status_line();

        # initialize the base class
        force._force.__init__(self,name)

        # create the c++ mirror class
        if not hoomd.context.exec_conf.isCUDAEnabled():
            self.cpp_force = _md.OrientationRestraintCompute(hoomd.context.current.system_definition,group.cpp_group,k)
        else:
            self.cpp_force = _md.OrientationRestraintComputeGPU(hoomd.context.current.system_definition,group.cpp_group,k)

        hoomd.context.current.system.addCompute(self.cpp_force, self.force_name)
    ## \internal
    # \var cpp_force
    # \brief C++ object for position restraint

    ## Set the force constants for the restraining potential
    # \param k Force constant
    #
    # \b Examples
    # \code
    # resor.set_params(k=3.0)
    # \endcode
    def set_params(self, k):
        self.cpp_force.setForceConstant(k)

    ## Set the reference orientations for the restraints
    #
    # \b Examples
    # \code
    # resor.set_reference_orientations(ref_orient=ref_orient)
    # \endcode
    def set_reference_orientations(self, ref_orient):
        # append each particle orientation
        for i in range(0, len(ref_orient)):
            self.set_orientation(i,ref_orient[i,:])

    ## \internal
    def set_orientation(self, i, orient):
        xyzw = hoomd.make_scalar4(orient[0],orient[1],orient[2],orient[3]);
        self.cpp_force.setOrientation(i,xyzw)
            
    ## \internal
    def update_coeffs(self):
        pass
