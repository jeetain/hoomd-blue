/*
Highly Optimized Object-oriented Many-particle Dynamics -- Blue Edition
(HOOMD-blue) Open Source Software License Copyright 2009-2014 The Regents of
the University of Michigan All rights reserved.

HOOMD-blue may contain modifications ("Contributions") provided, and to which
copyright is held, by various Contributors who have granted The Regents of the
University of Michigan the right to modify and/or distribute such Contributions.

You may redistribute, use, and create derivate works of HOOMD-blue, in source
and binary forms, provided you abide by the following conditions:

* Redistributions of source code must retain the above copyright notice, this
list of conditions, and the following disclaimer both in the code and
prominently in any materials provided with the distribution.

* Redistributions in binary form must reproduce the above copyright notice, this
list of conditions, and the following disclaimer in the documentation and/or
other materials provided with the distribution.

* All publications and presentations based on HOOMD-blue, including any reports
or published results obtained, in whole or in part, with HOOMD-blue, will
acknowledge its use according to the terms posted at the time of submission on:
http://codeblue.umich.edu/hoomd-blue/citations.html

* Any electronic documents citing HOOMD-Blue will link to the HOOMD-Blue website:
http://codeblue.umich.edu/hoomd-blue/

* Apart from the above required attributions, neither the name of the copyright
holder nor the names of HOOMD-blue's contributors may be used to endorse or
promote products derived from this software without specific prior written
permission.

Disclaimer

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER AND CONTRIBUTORS ``AS IS'' AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND/OR ANY
WARRANTIES THAT THIS SOFTWARE IS FREE OF INFRINGEMENT ARE DISCLAIMED.

IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

// Maintainer: mphoward

/*! \file PositionRestraintComputeGPU.cu
    \brief Defines CUDA kernels for PositionRestraintComputeGPU
*/

#include "PositionRestraintComputeGPU.cuh"

__global__ void gpu_compute_position_restraint_kernel(Scalar4 *d_force,
                                                      const unsigned int *d_member_idx,
                                                      const Scalar4 *d_pos,
						      const int3 *d_image,
                                                      const Scalar4 *d_ref_pos,
                                                      const unsigned int *d_tag,
                                                      const Scalar3 k,
                                                      const BoxDim box,
                                                      const unsigned int N_mem)
    {
    unsigned int idx;
    if (gridDim.y > 1)
        {
        // fermi workaround
        idx = (blockIdx.x + blockIdx.y*65535) * blockDim.x + threadIdx.x;
        }
    else
        {
        idx = blockIdx.x * blockDim.x + threadIdx.x;
        }

    // one thread per particle
    if (idx >= N_mem)
        return;
    
    const unsigned int cur_p = d_member_idx[idx];
    const Scalar4 cur_pos_type = d_pos[cur_p];
    const Scalar3 cur_pos = make_scalar3(cur_pos_type.x, cur_pos_type.y, cur_pos_type.z);

    const unsigned int cur_tag = d_tag[cur_p];
    const Scalar4 cur_ref_pos_type = d_ref_pos[cur_tag];
    const Scalar3 cur_ref_pos = make_scalar3(cur_ref_pos_type.x, cur_ref_pos_type.y, cur_ref_pos_type.z);

    // compute distance between current and reference position
    Scalar3 dr = box.minImage(cur_pos - cur_ref_pos);
    //   (better to use unwrapped coordinates for this displacement vector)
    // Scalar3 cur_pos_unwrapped = box.shift(cur_pos, d_image[cur_p]);
    // Scalar3 dr = cur_pos_unwrapped - cur_ref_pos;

    // termwise squaring for energy calculation
    const Scalar3 dr2 = make_scalar3(dr.x*dr.x, dr.y*dr.y, dr.z*dr.z);

    // F = -k x, U = 0.5 kx^2
    d_force[cur_p] = make_scalar4(-k.x*dr.x,
                                  -k.y*dr.y,
                                  -k.z*dr.z,
                                  Scalar(0.5)*dot(k, dr2));
    }

cudaError_t gpu_compute_position_restraint(Scalar4 *d_force,
                                           const unsigned int *d_member_idx,
                                           const Scalar4 *d_pos,
					   const int3 *d_image,
                                           const Scalar4 *d_ref_pos,
                                           const unsigned int *d_tag,
                                           const Scalar3& k,
                                           const BoxDim& box,
                                           const unsigned int N,
                                           const unsigned int N_mem,
                                           const unsigned int block_size,
                                           const unsigned int compute_capability)
    {
    // asynchronous memset in the default stream will allow other simple hosts tasks to proceed before kernel launch
    cudaError_t error;
    error = cudaMemsetAsync(d_force, 0, sizeof(Scalar4)*N, 0);

    if (error != cudaSuccess)
        return error;

    static unsigned int max_block_size = UINT_MAX;
    if (max_block_size == UINT_MAX)
        {
        cudaFuncAttributes attr;
        cudaFuncGetAttributes(&attr, (const void*)gpu_compute_position_restraint_kernel);
        max_block_size = attr.maxThreadsPerBlock;
        }

    unsigned int run_block_size = min(block_size, max_block_size);

    dim3 grid(N_mem / run_block_size + 1);
    if (compute_capability < 30 && grid.x > 65535) // Fermi workaround
        {
        grid.y = grid.x/65535 + 1;
        grid.x = 65535;
        }

    gpu_compute_position_restraint_kernel<<<grid, run_block_size>>>(d_force,
                                                                    d_member_idx,
                                                                    d_pos,
								    d_image,
                                                                    d_ref_pos,
                                                                    d_tag,
                                                                    k,
                                                                    box,
                                                                    N_mem);
    return cudaSuccess;
    }
