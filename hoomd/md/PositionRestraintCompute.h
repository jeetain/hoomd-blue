/*
Highly Optimized Object-oriented Many-particle Dynamics -- Blue Edition
(HOOMD-blue) Open Source Software License Copyright 2009-2014 The Regents of
the University of Michigan All rights reserved.

HOOMD-blue may contain modifications ("Contributions") provided, and to which
copyright is held, by various Contributors who have granted The Regents of the
University of Michigan the right to modify and/or distribute such Contributions.

You may redistribute, use, and create derivate works of HOOMD-blue, in source
and binary forms, provided you abide by the following conditions:

* Redistributions of source code must retain the above copyright notice, this
list of conditions, and the following disclaimer both in the code and
prominently in any materials provided with the distribution.

* Redistributions in binary form must reproduce the above copyright notice, this
list of conditions, and the following disclaimer in the documentation and/or
other materials provided with the distribution.

* All publications and presentations based on HOOMD-blue, including any reports
or published results obtained, in whole or in part, with HOOMD-blue, will
acknowledge its use according to the terms posted at the time of submission on:
http://codeblue.umich.edu/hoomd-blue/citations.html

* Any electronic documents citing HOOMD-Blue will link to the HOOMD-Blue website:
http://codeblue.umich.edu/hoomd-blue/

* Apart from the above required attributions, neither the name of the copyright
holder nor the names of HOOMD-blue's contributors may be used to endorse or
promote products derived from this software without specific prior written
permission.

Disclaimer

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER AND CONTRIBUTORS ``AS IS'' AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND/OR ANY
WARRANTIES THAT THIS SOFTWARE IS FREE OF INFRINGEMENT ARE DISCLAIMED.

IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

// Maintainer: mphoward

/*! \file PositionRestraintCompute.h
    \brief Declares a class for computing position restraining forces
*/

#ifdef NVCC
#error This header cannot be compiled by nvcc
#endif

#ifndef _POSITION_RESTRAINT_COMPUTE_H_
#define _POSITION_RESTRAINT_COMPUTE_H_

#include "hoomd/ForceCompute.h"

//! Adds a restraining force to groups of particles
/*! \ingroup computes
*/
class PositionRestraintCompute : public ForceCompute
    {
    public:
        //! Constructs the compute
        PositionRestraintCompute(std::shared_ptr<SystemDefinition> sysdef, std::shared_ptr<ParticleGroup> group, Scalar kx, Scalar ky, Scalar kz);

        //! Destructor
        ~PositionRestraintCompute();

        //! Set the force constant to a new value
        /*!
         * \param kx Force constant in x-direction
         * \param ky Force constant in y-direction
         * \param kz Force constant in z-direction
         */
        void setForceConstant(Scalar kx, Scalar ky, Scalar kz)
            {
            m_k = make_scalar3(kx, ky, kz);
            }

	//! Sets the reference positions of particles to their current value
	void setInitialPositions();
	
	//! Sets the reference position of a particles to a supplied value
        void setPosition(unsigned int tag, Scalar4 &pos);

        //! Returns a list of log quantities this compute calculates
        virtual std::vector< std::string > getProvidedLogQuantities();

        //! Calculates the requested log value and returns it
        virtual Scalar getLogValue(const std::string& quantity, unsigned int timestep);
	
    protected:
        
        //! Actually compute the forces
        virtual void computeForces(unsigned int timestep);

        Scalar3 m_k; //!< Force constants

        std::shared_ptr<ParticleGroup> m_group;  //!< Group of particles to apply force to
        GPUArray<Scalar4> m_ref_pos;               //!< Reference positions of the particles stored by tag
	std::vector< std::string > m_logname_list; //!< Cache all generated logged quantities names
    };

//! Exports the PositionRestraintCompute to python
void export_PositionRestraintCompute(pybind11::module& m);

#endif // _POSITION_RESTRAINT_COMPUTE_H_
