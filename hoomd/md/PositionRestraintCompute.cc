/*
Highly Optimized Object-oriented Many-particle Dynamics -- Blue Edition
(HOOMD-blue) Open Source Software License Copyright 2009-2014 The Regents of
the University of Michigan All rights reserved.

HOOMD-blue may contain modifications ("Contributions") provided, and to which
copyright is held, by various Contributors who have granted The Regents of the
University of Michigan the right to modify and/or distribute such Contributions.

You may redistribute, use, and create derivate works of HOOMD-blue, in source
and binary forms, provided you abide by the following conditions:

* Redistributions of source code must retain the above copyright notice, this
list of conditions, and the following disclaimer both in the code and
prominently in any materials provided with the distribution.

* Redistributions in binary form must reproduce the above copyright notice, this
list of conditions, and the following disclaimer in the documentation and/or
other materials provided with the distribution.

* All publications and presentations based on HOOMD-blue, including any reports
or published results obtained, in whole or in part, with HOOMD-blue, will
acknowledge its use according to the terms posted at the time of submission on:
http://codeblue.umich.edu/hoomd-blue/citations.html

* Any electronic documents citing HOOMD-Blue will link to the HOOMD-Blue website:
http://codeblue.umich.edu/hoomd-blue/

* Apart from the above required attributions, neither the name of the copyright
holder nor the names of HOOMD-blue's contributors may be used to endorse or
promote products derived from this software without specific prior written
permission.

Disclaimer

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER AND CONTRIBUTORS ``AS IS'' AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND/OR ANY
WARRANTIES THAT THIS SOFTWARE IS FREE OF INFRINGEMENT ARE DISCLAIMED.

IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

// Maintainer: mphoward

#ifdef WIN32
#pragma warning( push )
#pragma warning( disable : 4103 4244 )
#endif

#include "PositionRestraintCompute.h"

namespace py = pybind11;

#include <iostream>
using namespace std;

/*! \file PositionRestraintCompute.cc
    \brief Contains code for the PositionRestraintCompute class
*/
    
/*!
 * \param sysdef SystemDefinition containing the ParticleData to compute forces on
 * \param group A group of particles
 * \param kx Force constant in x-direction
 * \param ky Force constant in y-direction
 * \param kz Force constant in z-direction
 */
PositionRestraintCompute::PositionRestraintCompute(std::shared_ptr<SystemDefinition> sysdef, std::shared_ptr<ParticleGroup> group, Scalar kx, Scalar ky, Scalar kz)
        : ForceCompute(sysdef), m_group(group)
    {
    m_exec_conf->msg->notice(5) << "Constructing PositionRestraintCompute" << endl;

    GPUArray<Scalar4> ref_pos(m_pdata->getN(), m_exec_conf);
    m_ref_pos.swap(ref_pos);
    
    setForceConstant(kx, ky, kz);

    setInitialPositions();

    m_logname_list.push_back(string("restraint_position_energy"));
    
    }

PositionRestraintCompute::~PositionRestraintCompute()
    {
    m_exec_conf->msg->notice(5) << "Destroying PositionRestraintCompute" << endl;
    }

void PositionRestraintCompute::setInitialPositions()
    {
    assert(m_ref_pos);
    // assert(m_group);

    ArrayHandle<Scalar4> h_pos(m_pdata->getPositions(), access_location::host, access_mode::read);
    // ArrayHandle<int3> h_image(m_pdata->getImages(), access_location::host, access_mode::read);
    ArrayHandle<Scalar4> h_ref_pos(m_ref_pos, access_location::host, access_mode::overwrite);
    // ArrayHandle<unsigned int> h_member_idx(m_group->getIndexArray(), access_location::host, access_mode::read);
    
    // const BoxDim& box = m_pdata->getBox();

    // copy data to ref
    memcpy(h_ref_pos.data, h_pos.data, sizeof(Scalar4) * m_pdata->getN());

    // // unwrap coordinates of ref
    // for (unsigned int cur_idx = 0; cur_idx < m_group->getNumMembers(); ++cur_idx)
    //     {
    //     const unsigned int cur_p = h_member_idx.data[cur_idx];
    // 	const Scalar4 cur_ref_pos_type = h_ref_pos.data[cur_p];
    // 	const Scalar3 cur_ref_pos = make_scalar3(cur_ref_pos_type.x, cur_ref_pos_type.y, cur_ref_pos_type.z);
    // 	const Scalar3 cur_ref_pos_unwrapped = box.shift(cur_ref_pos,h_image.data[cur_p]);
    // 	h_ref_pos.data[cur_p] = make_scalar4(cur_ref_pos_unwrapped.x,cur_ref_pos_unwrapped.y,cur_ref_pos_unwrapped.z,0.0);
    // 	}
    
    }

void PositionRestraintCompute::setPosition(unsigned int tag, Scalar4 &pos)
    {
    ArrayHandle<Scalar4> h_ref_pos(m_ref_pos, access_location::host, access_mode::overwrite);
    h_ref_pos.data[tag] = pos;
    }

std::vector< std::string > PositionRestraintCompute::getProvidedLogQuantities()
    {
    return m_logname_list;
    }

Scalar PositionRestraintCompute::getLogValue(const std::string& quantity, unsigned int timestep)
    {
    compute(timestep);
    if (quantity == m_logname_list[0])
        {
	return calcEnergySum();
        }
    else
        {
        m_exec_conf->msg->error() << "restrain.position: " << quantity << " is not a valid log quantity for PositionRestraintCompute" << endl;
        throw runtime_error("Error getting log value");
        }
    }

/*!
 * \param timestep Current timestep
 */
void PositionRestraintCompute::computeForces(unsigned int timestep)
    {

    ArrayHandle<Scalar4> h_force(m_force, access_location::host, access_mode::overwrite);
    ArrayHandle<Scalar> h_virial(m_virial, access_location::host, access_mode::overwrite);
    // zero the forces and virial
    memset((void*)h_force.data, 0, sizeof(Scalar4) * m_force.getNumElements());
    memset((void*)h_virial.data, 0, sizeof(Scalar) * m_virial.getNumElements());

    // the logger is dumb and always thinks virial will be computed
    // PDataFlags flags = m_pdata->getFlags();
    // bool compute_virial = flags[pdata_flag::pressure_tensor] || flags[pdata_flag::isotropic_virial];

    // if (compute_virial)
    //     {
    //     m_exec_conf->msg->error() << "Restraints with virial not supported" << endl;
    // 	throw runtime_error("Restraint virial calculation is not implemented");
    //     }
    
    ArrayHandle<Scalar4> h_ref_pos(m_ref_pos, access_location::host, access_mode::read);
    ArrayHandle<unsigned int> h_tag(m_pdata->getTags(), access_location::host, access_mode::read);
    ArrayHandle<Scalar4> h_pos(m_pdata->getPositions(), access_location::host, access_mode::read);
    // ArrayHandle<int3> h_image(m_pdata->getImages(), access_location::host, access_mode::read);
    ArrayHandle<unsigned int> h_member_idx(m_group->getIndexArray(), access_location::host, access_mode::read);

    const BoxDim& box = m_pdata->getBox();
    for (unsigned int cur_idx = 0; cur_idx < m_group->getNumMembers(); ++cur_idx)
        {
        const unsigned int cur_p = h_member_idx.data[cur_idx];
        const Scalar4 cur_pos_type = h_pos.data[cur_p];
        const Scalar3 cur_pos = make_scalar3(cur_pos_type.x, cur_pos_type.y, cur_pos_type.z);

        const unsigned int cur_tag = h_tag.data[cur_p];
        const Scalar4 cur_ref_pos_type = h_ref_pos.data[cur_tag];
        const Scalar3 cur_ref_pos = make_scalar3(cur_ref_pos_type.x, cur_ref_pos_type.y, cur_ref_pos_type.z);

        // compute distance between current and initial position
	Scalar3 dr = box.minImage(cur_pos - cur_ref_pos);
	//   (better to use unwrapped coordinates for this displacement vector)
	// Scalar3 cur_pos_unwrapped = box.shift(cur_pos, h_image.data[cur_idx]);
        // Scalar3 dr = cur_pos_unwrapped - cur_ref_pos;

        // termwise squaring for energy calculation
        const Scalar3 dr2 = make_scalar3(dr.x*dr.x, dr.y*dr.y, dr.z*dr.z);

        const Scalar3 force = make_scalar3(-m_k.x*dr.x, -m_k.y*dr.y, -m_k.z*dr.z);
	
        // F = -k x, U = 0.5 kx^2
        h_force.data[cur_p] = make_scalar4(force.x,
                                           force.y,
                                           force.z,
                                           Scalar(0.5)*dot(m_k, dr2));
        }
    }

void export_PositionRestraintCompute(py::module& m)
    {
    py::class_< PositionRestraintCompute, std::shared_ptr<PositionRestraintCompute> >(m, "PositionRestraintCompute", py::base<ForceCompute>() )
	.def(py::init< std::shared_ptr<SystemDefinition>, std::shared_ptr<ParticleGroup>, Scalar, Scalar, Scalar >())
	.def("setForceConstant", &PositionRestraintCompute::setForceConstant)
	.def("setPosition", &PositionRestraintCompute::setPosition)
    ;
    }

#ifdef WIN32
#pragma warning( pop )
#endif
