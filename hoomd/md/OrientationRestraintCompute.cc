/*
Highly Optimized Object-oriented Many-particle Dynamics -- Blue Edition
(HOOMD-blue) Open Source Software License Copyright 2009-2014 The Regents of
the University of Michigan All rights reserved.

HOOMD-blue may contain modifications ("Contributions") provided, and to which
copyright is held, by various Contributors who have granted The Regents of the
University of Michigan the right to modify and/or distribute such Contributions.

You may redistribute, use, and create derivate works of HOOMD-blue, in source
and binary forms, provided you abide by the following conditions:

* Redistributions of source code must retain the above copyright notice, this
list of conditions, and the following disclaimer both in the code and
prominently in any materials provided with the distribution.

* Redistributions in binary form must reproduce the above copyright notice, this
list of conditions, and the following disclaimer in the documentation and/or
other materials provided with the distribution.

* All publications and presentations based on HOOMD-blue, including any reports
or published results obtained, in whole or in part, with HOOMD-blue, will
acknowledge its use according to the terms posted at the time of submission on:
http://codeblue.umich.edu/hoomd-blue/citations.html

* Any electronic documents citing HOOMD-Blue will link to the HOOMD-Blue website:
http://codeblue.umich.edu/hoomd-blue/

* Apart from the above required attributions, neither the name of the copyright
holder nor the names of HOOMD-blue's contributors may be used to endorse or
promote products derived from this software without specific prior written
permission.

Disclaimer

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER AND CONTRIBUTORS ``AS IS'' AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND/OR ANY
WARRANTIES THAT THIS SOFTWARE IS FREE OF INFRINGEMENT ARE DISCLAIMED.

IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

// Maintainer: mphoward

#ifdef WIN32
#pragma warning( push )
#pragma warning( disable : 4103 4244 )
#endif

#include "OrientationRestraintCompute.h"

#include "QuaternionMath.h"
#include "hoomd/HOOMDMath.h"

namespace py = pybind11;

#include <iostream>
using namespace std;

/*! \file OrientationRestraintCompute.cc
    \brief Contains code for the OrientationRestraintCompute class
*/
    
/*!
 * \param sysdef SystemDefinition containing the ParticleData to compute torques on
 * \param group A group of particles
 * \param k Force constant
 */
OrientationRestraintCompute::OrientationRestraintCompute(std::shared_ptr<SystemDefinition> sysdef, std::shared_ptr<ParticleGroup> group, Scalar k)
        : ForceCompute(sysdef), m_group(group)
    {
    m_exec_conf->msg->notice(5) << "Constructing OrientationRestraintCompute" << endl;

    GPUArray<Scalar4> ref_orient(m_pdata->getN(), m_exec_conf);
    m_ref_orient.swap(ref_orient);
    
    setForceConstant(k);

    setInitialOrientations();

    m_logname_list.push_back(string("restraint_orientation_energy"));
    
    }

OrientationRestraintCompute::~OrientationRestraintCompute()
    {
    m_exec_conf->msg->notice(5) << "Destroying OrientationRestraintCompute" << endl;
    }

void OrientationRestraintCompute::setInitialOrientations()
    {
    assert(m_ref_orient);

    ArrayHandle<Scalar4> h_orient(m_pdata->getOrientationArray(), access_location::host, access_mode::read);
    ArrayHandle<Scalar4> h_ref_orient(m_ref_orient, access_location::host, access_mode::overwrite);

    memcpy(h_ref_orient.data, h_orient.data, sizeof(Scalar4) * m_pdata->getN());
    }

void OrientationRestraintCompute::setOrientation(unsigned int tag, Scalar4 &orient)
    {
    ArrayHandle<Scalar4> h_ref_orient(m_ref_orient, access_location::host, access_mode::overwrite);
    h_ref_orient.data[tag] = orient;
    }

/*! Sums the total potential energy calculated by the last call to compute() and returns it.
*/
Scalar OrientationRestraintCompute::calcOrientationEnergySum()
    {
    ArrayHandle<Scalar4> h_torque(m_torque,access_location::host,access_mode::read);
    double pe_total = 0.0;
    for (unsigned int i=0; i < m_pdata->getN(); i++)
        {
        pe_total += (double)h_torque.data[i].w;
        }
#ifdef ENABLE_MPI
    if (m_comm)
        {
        // reduce potential energy on all processors
        MPI_Allreduce(MPI_IN_PLACE, &pe_total, 1, MPI_DOUBLE, MPI_SUM, m_exec_conf->getMPICommunicator());
        }
#endif
    return Scalar(pe_total);
    }

std::vector< std::string > OrientationRestraintCompute::getProvidedLogQuantities()
    {
    return m_logname_list;
    }

Scalar OrientationRestraintCompute::getLogValue(const std::string& quantity, unsigned int timestep)
    {
    compute(timestep);
    if (quantity == m_logname_list[0])
        {
	return calcOrientationEnergySum();
        }
    else
        {
        m_exec_conf->msg->error() << "restrain.orientation: " << quantity << " is not a valid log quantity for OrientationRestraintCompute" << endl;
        throw runtime_error("Error getting log value");
        }
    }

/*!
 * \param timestep Current timestep
 */
void OrientationRestraintCompute::computeForces(unsigned int timestep)
    {

    ArrayHandle<Scalar4> h_torque(m_torque, access_location::host, access_mode::overwrite);
    ArrayHandle<Scalar> h_virial(m_virial, access_location::host, access_mode::overwrite);
    // zero the torques and virial
    memset((void*)h_torque.data, 0, sizeof(Scalar4) * m_torque.getNumElements());
    memset((void*)h_virial.data, 0, sizeof(Scalar) * m_virial.getNumElements());
    
    ArrayHandle<Scalar4> h_ref_orient(m_ref_orient, access_location::host, access_mode::read);
    ArrayHandle<unsigned int> h_tag(m_pdata->getTags(), access_location::host, access_mode::read);
    ArrayHandle<Scalar4> h_orient(m_pdata->getOrientationArray(), access_location::host, access_mode::read);
    ArrayHandle<unsigned int> h_member_idx(m_group->getIndexArray(), access_location::host, access_mode::read);

    for (unsigned int cur_idx = 0; cur_idx < m_group->getNumMembers(); ++cur_idx)
        {
        const unsigned int cur_p = h_member_idx.data[cur_idx];
        const Scalar4 cur_orient = h_orient.data[cur_p];

        const unsigned int cur_tag = h_tag.data[cur_p];
        const Scalar4 cur_ref_orient = h_ref_orient.data[cur_tag];

	// convert patch vector in the body frame of each particle to space frame
	vec3<Scalar> n_i = rotate(quat<Scalar>(cur_orient), vec3<Scalar>(1.0, 0, 0));
	vec3<Scalar> n_ref = rotate(quat<Scalar>(cur_ref_orient), vec3<Scalar>(1.0, 0, 0));
	
        // compute angle between current and initial orientation
        Scalar orient_dot = dot(n_i,n_ref);

	// compute energy
	// U = k * sin(theta)^2
	//   = k * [ 1 - cos(theta)^2 ]
	//   = k * [ 1 - (n_i \dot n_ref)^2 ]
	Scalar energy = m_k * ( Scalar(1.0) - orient_dot * orient_dot );
	
	// compute torque
	// T = -dU/d(n_i \dot n_ref) * (n_i x n_ref)
	//   = -k * [ 1 - 2 (n_i \dot n_ref) ] * (n_i x n_ref)
	const Scalar dUddot = ( Scalar(1.0) - Scalar(2.0) * orient_dot );
	vec3<Scalar> torque_dir = cross(n_i,n_ref);
	const Scalar3 torque = vec_to_scalar3(Scalar(-1.0) * m_k * dUddot * torque_dir );
	
        h_torque.data[cur_p] = make_scalar4(torque.x,
                                            torque.y,
                                            torque.z,
                                            energy);
        }
    }

void export_OrientationRestraintCompute(py::module& m)
    {
    py::class_< OrientationRestraintCompute, std::shared_ptr<OrientationRestraintCompute> >(m, "OrientationRestraintCompute", py::base<ForceCompute>() )
	.def(py::init< std::shared_ptr<SystemDefinition>, std::shared_ptr<ParticleGroup>, Scalar >())
	.def("setForceConstant", &OrientationRestraintCompute::setForceConstant)
	.def("setOrientation", &OrientationRestraintCompute::setOrientation)
    ;
    }

#ifdef WIN32
#pragma warning( pop )
#endif
