// Copyright (c) 2009-2016 The Regents of the University of Michigan
// This file is part of the HOOMD-blue project, released under the BSD 3-Clause License.


// Maintainer: joaander

#ifndef __PAIR_EVALUATOR_JAGLA_H__
#define __PAIR_EVALUATOR_JAGLA_H__

#ifndef NVCC
#include <string>
//#include <iostream>
//#include <stdexcept>
//#include <boost/python.hpp>
#endif

#include "hoomd/HOOMDMath.h"

// need to declare these class methods with __device__ qualifiers when building in nvcc
// DEVICE is __host__ __device__ when included in nvcc and blank when included into the host compiler
#ifdef NVCC
#define DEVICE __device__
#define HOSTDEVICE __host__ __device__
#else
#define DEVICE
#define HOSTDEVICE
#endif

//#ifndef NVCC
//using namespace boost::python;
//#endif

//! Parameter type for this potential
struct jagla_params
    {
      Scalar epsilon;
      Scalar sigma;
      Scalar rshift;
      Scalar n;
      Scalar A0;
      Scalar A1;
      Scalar A2;
      Scalar B0;
      Scalar B1;
      Scalar B2;      
};

//! Function to make the parameter type
HOSTDEVICE inline jagla_params make_jagla_params(Scalar epsilon,
					     Scalar sigma,
					     Scalar rshift,
					     Scalar n,
					     Scalar A0,
					     Scalar A1,
					     Scalar A2,
					     Scalar B0,
					     Scalar B1,
					     Scalar B2)
    {
    jagla_params retval;
    retval.epsilon = epsilon;
    retval.sigma   = sigma;
    retval.rshift  = rshift;
    retval.n       = n;
    retval.A0      = A0;
    retval.A1      = A1;
    retval.A2      = A2;
    retval.B0      = B0;
    retval.B1      = B1;
    retval.B2      = B2;    
    return retval;
    }

class EvaluatorPairJagla
    {
    public:
        //! Define the parameter type used by this pair potential evaluator
        typedef jagla_params param_type;

        //! Constructs the pair potential evaluator
        /*! \param _rsq Squared distance beteen the particles
            \param _rcutsq Sqauared distance at which the potential goes to 0
            \param _params Per type pair parameters of this potential
        */
        DEVICE EvaluatorPairJagla(Scalar _rsq, Scalar _rcutsq, const param_type& _params)
	  : rsq(_rsq), rcutsq(_rcutsq), epsilon(_params.epsilon), sigma(_params.sigma),  rshift(_params.rshift),  n(_params.n),
	    A0(_params.A0), A1(_params.A1), A2(_params.A2), B0(_params.B0), B1(_params.B1), B2(_params.B2)
            {
            }

        DEVICE static bool needsDiameter() { return false; }
        //! Accept the optional diameter values
        /*! \param di Diameter of particle i
            \param dj Diameter of particle j
        */
        DEVICE void setDiameter(Scalar di, Scalar dj) { }

        DEVICE static bool needsCharge() { return false; }
        //! Accept the optional diameter values
        /*! \param qi Charge of particle i
            \param qj Charge of particle j
        */
        DEVICE void setCharge(Scalar qi, Scalar qj) { }

        //! Evaluate the force and energy
        /*! \param force_divr Output parameter to write the computed force divided by r.
            \param pair_eng Output parameter to write the computed pair energy
            \param energy_shift If true, the potential must be shifted so that V(r) is continuous at the cutoff
            \note There is no need to check if rsq < rcutsq in this method. Cutoff tests are performed
                  in PotentialPair.

            \return True if they are evaluated or false if they are not because we are beyond the cuttoff
        */
        DEVICE bool evalForceAndEnergy(Scalar& force_divr, Scalar& pair_eng, bool energy_shift)
            {
            // compute the force divided by r in force_divr
            if (rsq < rcutsq )
                {
                Scalar r = sqrt(rsq);

                Scalar n_term  = epsilon * pow( sigma / ( r - rshift) , n );
                Scalar A_exp   = exp( A1 * (r - A2) );
                Scalar A_denom = Scalar(1.0) / ( Scalar(1.0) + A_exp );
                Scalar B_exp   = exp( B1 * (r - B2) );
                Scalar B_denom = Scalar(1.0) / ( Scalar(1.0) + B_exp );
                pair_eng = n_term + A0 * A_denom - B0 * B_denom;

                Scalar d_n_term_dr = -n / (r-rshift) * n_term;
                Scalar d_A_term_dr = -( A_denom * A_denom ) * A0 * A1 * A_exp;
                Scalar d_B_term_dr = -( B_denom * B_denom ) * B0 * B1 * B_exp;
                force_divr = -(d_n_term_dr + d_A_term_dr - d_B_term_dr) / r;
		
                if (energy_shift)
                  {
                  Scalar rcut = fast::sqrt(rcutsq);
                  Scalar shift_n_term = epsilon * pow( sigma / ( rcut - rshift) , n );
                  Scalar shift_A_term = A0 / ( Scalar(1.0) + exp( A1 * (rcut - A2) ) );
                  Scalar shift_B_term = B0 / ( Scalar(1.0) + exp( B1 * (rcut - B2) ) );
                  pair_eng -= shift_n_term + shift_A_term - shift_B_term;
                  }
                return true;
                }
            else
              return false;
            }

        #ifndef NVCC
        //! Get the name of this potential
        /*! \returns The potential name. Must be short and all lowercase, as this is the name energies will be logged as
            via analyze.log.
        */
        static std::string getName()
            {
            return std::string("jagla");
            }
        #endif

    protected:
        Scalar rsq;     //!< Stored rsq from the constructor
        Scalar rcutsq;  //!< Stored rcutsq from the constructor
	Scalar epsilon, sigma, rshift, n, A0, A1, A2, B0, B1, B2;
    };

#undef DEVICE
#undef HOSTDEVICE

#endif // __PAIR_EVALUATOR_JAGLA_H__
