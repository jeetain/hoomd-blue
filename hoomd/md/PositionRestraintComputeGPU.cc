/*
Highly Optimized Object-oriented Many-particle Dynamics -- Blue Edition
(HOOMD-blue) Open Source Software License Copyright 2009-2014 The Regents of
the University of Michigan All rights reserved.

HOOMD-blue may contain modifications ("Contributions") provided, and to which
copyright is held, by various Contributors who have granted The Regents of the
University of Michigan the right to modify and/or distribute such Contributions.

You may redistribute, use, and create derivate works of HOOMD-blue, in source
and binary forms, provided you abide by the following conditions:

* Redistributions of source code must retain the above copyright notice, this
list of conditions, and the following disclaimer both in the code and
prominently in any materials provided with the distribution.

* Redistributions in binary form must reproduce the above copyright notice, this
list of conditions, and the following disclaimer in the documentation and/or
other materials provided with the distribution.

* All publications and presentations based on HOOMD-blue, including any reports
or published results obtained, in whole or in part, with HOOMD-blue, will
acknowledge its use according to the terms posted at the time of submission on:
http://codeblue.umich.edu/hoomd-blue/citations.html

* Any electronic documents citing HOOMD-Blue will link to the HOOMD-Blue website:
http://codeblue.umich.edu/hoomd-blue/

* Apart from the above required attributions, neither the name of the copyright
holder nor the names of HOOMD-blue's contributors may be used to endorse or
promote products derived from this software without specific prior written
permission.

Disclaimer

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER AND CONTRIBUTORS ``AS IS'' AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND/OR ANY
WARRANTIES THAT THIS SOFTWARE IS FREE OF INFRINGEMENT ARE DISCLAIMED.

IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

// Maintainer: mphoward

#ifdef WIN32
#pragma warning( push )
#pragma warning( disable : 4103 4244 )
#endif

#include "PositionRestraintComputeGPU.h"
#include "PositionRestraintComputeGPU.cuh"

namespace py = pybind11;

using namespace std;


/*! \file PositionRestraintCompute.cc
    \brief Contains code for the PositionRestraintCompute class
*/
    
/*!
 * \param sysdef SystemDefinition containing the ParticleData to compute forces on
 * \param group A group of particles
 * \param kx Force constant in x-direction
 * \param ky Force constant in y-direction
 * \param kz Force constant in z-direction
 */
PositionRestraintComputeGPU::PositionRestraintComputeGPU(std::shared_ptr<SystemDefinition> sysdef,
                                                         std::shared_ptr<ParticleGroup> group,
                                                         Scalar kx,
                                                         Scalar ky,
                                                         Scalar kz)
        : PositionRestraintCompute(sysdef, group, kx, ky, kz)
    {
    m_tuner.reset(new Autotuner(32, 1024, 32, 5, 100000, "position_restraint", this->m_exec_conf));
    }

/*!
 * \param timestep Current timestep
 */
void PositionRestraintComputeGPU::computeForces(unsigned int timestep)
    {
    ArrayHandle<Scalar4> d_force(m_force,access_location::device,access_mode::overwrite);

    ArrayHandle<unsigned int> d_member_idx(m_group->getIndexArray(), access_location::device, access_mode::read);
    ArrayHandle<Scalar4> d_ref_pos(m_ref_pos, access_location::device, access_mode::read);
    ArrayHandle<unsigned int> d_tag(m_pdata->getTags(), access_location::device, access_mode::read);
    ArrayHandle<Scalar4> d_pos(m_pdata->getPositions(), access_location::device, access_mode::read);
    ArrayHandle<int3> d_image(m_pdata->getImages(), access_location::device, access_mode::read);

    m_tuner->begin();
    gpu_compute_position_restraint(d_force.data,
                                   d_member_idx.data,
                                   d_pos.data,
				   d_image.data,
                                   d_ref_pos.data,
                                   d_tag.data,
                                   m_k,
                                   m_pdata->getBox(),
                                   m_pdata->getN(),
                                   m_group->getNumMembers(),
                                   m_tuner->getParam(),
                                   m_exec_conf->getComputeCapability()/10);
    if (m_exec_conf->isCUDAErrorCheckingEnabled()) CHECK_CUDA_ERROR();
    m_tuner->end();
    }

void export_PositionRestraintComputeGPU(py::module& m)
    {
    py::class_< PositionRestraintComputeGPU, std::shared_ptr<PositionRestraintComputeGPU> >(m, "PositionRestraintComputeGPU", py::base<PositionRestraintCompute>() )
	.def(py::init< std::shared_ptr<SystemDefinition>, std::shared_ptr<ParticleGroup>, Scalar, Scalar, Scalar >())
    ;
    }

#ifdef WIN32
#pragma warning( pop )
#endif
